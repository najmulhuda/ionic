import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	
	@ViewChild('username') uname;
	@ViewChild('password') password;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {

  }
  
  signIn()
  {
	  if(this.uname.value == "admin" && this.password.value == "admin")
	  {
		  let alert = this.alertCtrl.create({
			  title:"Login successful!",
			  subTitle: 'You are Logged in',
			  buttons: ['OK']
		  });
		  alert.present();
	  }
	  else
	  {
		  let alert = this.alertCtrl.create({
			  title:"Login Failed!",
			  subTitle: 'You are failed!!!',
			  buttons: ['OK']
		  });
		  alert.present();
	  }
  }

}
